#!/bin/bash

echo -e "\033[01mWARNING: Docker alternative untested, put here for conveniance."
echo -e "\033[01mFor some reason, the Java runtime freezes when ran within a Linux/amd64 Docker Container from a M1 MacOSX Docker runtime."
echo -e "\033[00m"

swagger-codegen generate \
	-l go-server \
	-i ./api/swagger.yaml \
	-o ./tmp \
	|| \
docker run --rm -v ${PWD}:/local swaggerapi/swagger-codegen-cli-v3 generate \
	-l go-server \
	-i ./api/swagger.yaml \
	-o ./tmp

swagger-codegen generate \
	-l go \
	-i ./api/swagger.yaml \
	-o ./tmp/client \
	--api-package client \
	--invoker-package client \
	|| \
docker run --rm -v ${PWD}:/local swaggerapi/swagger-codegen-cli-v3 generate \
	-l go \
	-i ./api/swagger.yaml \
	-o ./tmp/client \
	--api-package client \
	--invoker-package client

echo -e "\033[0m"
echo -e "\033[01mSorting up generated server files…"

# Cleanup existing generated documentation
rm -rf ./api/docs

# Import generated files, without overwriting existing files
cp -n ./tmp/go/* ./cmd/webserver/
mv ./tmp/client/docs ./api/docs

# The purpose is to rename the generated package name to be immediately usable with no manual modification.
for file in $(echo ./cmd/webserver/*.go)
do
	# Using `ed(1)` instead of `sed(1)` because `ed(1)` is the only true standard editor }:->
	ed $file > /dev/null <<EOF
,s/package swagger/package main/g
w
q
EOF
done

rm -r ./tmp

echo -e "\033[01mAPI generated.\033[0m"
