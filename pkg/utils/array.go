package util

// Map array to another type, by applying given transformer function.
func Map[T any, U any](arr []T, f func(T) U) []U {
	var ret = make([]U, len(arr))

	for i := range arr {
		ret[i] = f(arr[i])
	}
	return ret
}
