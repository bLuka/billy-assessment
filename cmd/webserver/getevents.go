package main

import (
	"encoding/json"
	"net/http"
	"strconv"
	"time"

	"gitlab.com/bLuka/billy-assessment/internal/store/event"
	"gitlab.com/bLuka/billy-assessment/internal/store/smartcontract"
	util "gitlab.com/bLuka/billy-assessment/pkg/utils"
)

// EventsGet handles the request to dump the whole event list through GET /events
// If a startTime is provided, the result should only contain events whose saleStartTime is _after_ the provided time.
func EventsGet(w http.ResponseWriter, r *http.Request) {
	filter := func(o event.OrganizerEvent) bool {
		return true
	}
	t := r.URL.Query().Get("saleStartTime")
	if t != "" {
		timestamp, err := strconv.ParseInt(t, 10, 64)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		startTime := time.Unix(timestamp, 0)

		filter = func(o event.OrganizerEvent) bool {
			return o.SaleStartDate.After(startTime)
		}
	}

	events := util.Map(organizerEventsStore.List(filter), func(src event.OrganizerEvent) Event {
		e := Event{
			EventID:           src.ID,
			Title:             src.Title,
			StartDatetime:     src.StartDate,
			EndDateTime:       src.EndDate,
			TotalTicketsCount: int64(src.TicketsCount),
			LineUp:            src.LineUp,
			AssetURL:          src.MediaURL,
			LocationName:      src.Location.Name,
			Address:           src.Location.Address,
		}

		return e
	})
	for i := range events {
		contracts := util.Map(smartcontractsStore.Get(events[i].EventID), func(c smartcontract.Contract) TicketCollectionInner {
			return TicketCollectionInner{
				CollectionName:    c.CollectionName,
				ScAddress:         c.CrowdsaleAddr, // Not sure that's the right match there!
				CollectionAddress: c.CollectionAddr,
				PricePerToken:     c.SaleParams.PricePerToken,
				MaxMintPerUser:    c.SaleParams.MaxMintPerUser,
				SaleSize:          int64(c.SaleParams.SaleSize),
			}
		})
		events[i].TicketCollections = &contracts
	}

	jsonData, err := json.Marshal(events)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.Write(jsonData)
}
