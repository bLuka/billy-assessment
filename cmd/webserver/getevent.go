package main

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/bLuka/billy-assessment/internal/store/event"
	"gitlab.com/bLuka/billy-assessment/internal/store/smartcontract"
	util "gitlab.com/bLuka/billy-assessment/pkg/utils"
)

// EventEventIDGet handles single event retrieval through GET /event/{eventID}
func EventEventIDGet(w http.ResponseWriter, r *http.Request) {
	eventID, ok := mux.Vars(r)["eventID"]
	if !ok {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	e, err := organizerEventsStore.Get(eventID)
	if err == event.ErrNoEventFound {
		w.WriteHeader(http.StatusNotFound)
		return
	}
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		log.Println("Unexpected error:", err)
		return
	}

	contracts := util.Map(smartcontractsStore.Get(e.ID), func(c smartcontract.Contract) TicketCollectionInner {
		return TicketCollectionInner{
			CollectionName:    c.CollectionName,
			ScAddress:         c.CrowdsaleAddr, // Not sure that's the right match there!
			CollectionAddress: c.CollectionAddr,
			PricePerToken:     c.SaleParams.PricePerToken,
			MaxMintPerUser:    c.SaleParams.MaxMintPerUser,
			SaleSize:          int64(c.SaleParams.SaleSize),
		}
	})

	jsonData, err := json.Marshal(Event{
		e.ID,
		e.Title,
		e.StartDate,
		e.EndDate,
		e.Location.Address,
		e.Location.Name,
		int64(e.TicketsCount),
		e.MediaURL,
		e.LineUp,
		&contracts,
	})
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.Write(jsonData)
}
