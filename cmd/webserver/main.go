package main

import (
	"log"
	"net/http"

	"gitlab.com/bLuka/billy-assessment/internal/store/event"
	"gitlab.com/bLuka/billy-assessment/internal/store/smartcontract"
)

const addr = ":8080"

// organizerEventsStore is defined as a package global to allow easy access from the API.
// This is a pragmatic-short-term approach which suits this context (and often suits small code-base applications), but
// when codebase is growing and getting more and more complex, I like to consider an API structure containing all its
// dependencies, injected either manually or through a dependency injection tool (such as github.com/google/wire).
// So please don't blame me for being lazy there :)
var (
	organizerEventsStore = event.NewMemoryStore()
	smartcontractsStore  = smartcontract.NewMemoryStore()
)

func init() {
	err := event.Populate(organizerEventsStore, "./organizers-data.csv")
	if err != nil {
		panic(err)
	}

	err = smartcontract.Populate(smartcontractsStore, "./smart-contracts-data.json")
	if err != nil {
		panic(err)
	}
}

func main() {
	r := NewRouter()

	log.Println("Listening on address", addr)
	http.ListenAndServe(addr, r)
}
