# Submission

The project has been done in Go, and the repository uses the [Golang Project Layout](https://github.com/golang-standards/project-layout).

## Usage

### Dependencies

Everything is meant to be runnable using Docker. It is also possible to run everything if you have the Go binaries
installed.

### Documentation

The API documentation can be accessed through Swagger by running `./swagger.sh`. It is the elegant way of browsing `./api/swagger.yaml`.

Markdown API documentation has also been generated in [`./api/docs/`](./api/docs).

### Building

You can run either `docker build -t webserver .` (Docker) or `go build ./cmd/...`.

### Running

You can run `docker run --rm -p 8080:8080 webserver`. If using Golang, run the previously built binary (`./webserver`)
or launch inline using `go run ./cmd/webserver`.

### API testing

I had some fun using Swagger web interface interactively for this. Feel free to do so using the `./swagger.sh`!

## Observations

I had quite some fun playing a bit with OpenAPI, though I'm very disappointed by the server code generation capabilities.
In the end, it's quite manual, and I had to go through most of the generated code for minor adjustments or bug fixes.

I tried to apply my (little) knowledge of DDD and Clean Architecture to the project. I tried to decouple the API logic
from the business logic, and the storage logic from the rest too. The idea is to keep the different user interfaces in
`./cmd`, the core logic within `./internal`, and, ultimately, move the memorystores logic within a dedicated and
decoupled package. I haven't gone that fare though, because it felt a little bit overkill for such a simple store
implementation.

Regarding code cleaning, I tried to do my best withough spending too much time.

However, I am aware I lack:
- Proper Go documentation for each exported method.
- Proper file naming, and eventually variable naming (I'm not bad at finding good names, but I'm not quick to do so
  neither).
- Proper business definition; this is something I tried to put in `./internal`, but it's a bit confused with store
  implementations. I tried to make sure to decouple schema definitions from API, from business, and from store logic,
  as much as I could. But it would have been better to move it in dedicated `model` packages, for instance.
- Clear API guidelines/explicit errors/… I have the bad habit of trying to obfuscate error messages from API, as I used
  to work on internal APIs exposed over the internet for millions of concurrent users. I realized a bit late, but could
  have done better.
- Better knowledge over the smartcontracts concepts. It has been a little bit blurry, causing me some troubles
  understanding the business purpose of some fields/relationships. Thus leaving me some surprises for the end :)
- Some time making sure everything would work in every environments (though I cleaned up Docker, and native Go support).
  I would have preferred writing down some scripts thoroughly tested on both Linux and OSX. Hope Docker works without
  any surprise there, fingers crossed!

Surely miss tons of other details you'll have a better time finding for me :)

I hope the review won't be too much uncomfortable in Go!
