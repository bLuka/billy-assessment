#!/bin/bash

docker pull swaggerapi/swagger-ui
docker run --name swagger --rm -p 8081:8080 -e SWAGGER_JSON=/swagger.yaml -v $(pwd)/api/swagger.yaml:/swagger.yaml swaggerapi/swagger-ui &

sleep 5
echo -e "\033[1mVisit the Swagger documentation on http://localhost:8081\033[0m"

# Try to open it automatically
xdg-open http://localhost:8081 2>/dev/null || open http://localhost:8081 2>/dev/null

wait
