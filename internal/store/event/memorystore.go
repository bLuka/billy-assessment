package event

import "sync"

type memoryStore struct {
	m *sync.Map
}

// NewMemoryStore instanciates a new in-memory store (no data will be persisted!)
func NewMemoryStore() Store {
	return &memoryStore{
		m: new(sync.Map),
	}
}

func (s *memoryStore) Add(e OrganizerEvent) error {
	s.m.Store(e.ID, e)
	return nil
}

func (s *memoryStore) Update(u OrganizerEventUpdate) error {
	v, ok := s.m.Load(u.ID)
	if !ok {
		return ErrNoEventFound
	}

	e := v.(OrganizerEvent)

	if u.Title != nil {
		e.Title = *u.Title
	}
	if u.StartDate != nil {
		e.StartDate = *u.StartDate
	}
	if u.EndDate != nil {
		e.EndDate = *u.EndDate
	}
	if u.Location.Name != nil {
		e.Location.Name = *u.Location.Name
	}
	if u.Location.Address != nil {
		e.Location.Address = *u.Location.Address
	}
	if u.TicketsCount != nil {
		e.TicketsCount = *u.TicketsCount
	}
	if u.MaximumTicketPerUser != nil {
		e.MaximumTicketPerUser = *u.MaximumTicketPerUser
	}
	if u.SaleStartDate != nil {
		e.SaleStartDate = *u.SaleStartDate
	}
	if u.LineUp != nil {
		e.LineUp = *u.LineUp
	}
	if u.MediaURL != nil {
		e.MediaURL = *u.MediaURL
	}

	s.m.Store(u.ID, e)

	return nil
}

func (s *memoryStore) Get(id string) (OrganizerEvent, error) {
	v, ok := s.m.Load(id)
	if !ok {
		return OrganizerEvent{}, ErrNoEventFound
	}
	return v.(OrganizerEvent), nil
}

func (s *memoryStore) List(filter func(OrganizerEvent) bool) []OrganizerEvent {
	list := make([]OrganizerEvent, 0)

	s.m.Range(func(_, value interface{}) bool {
		if filter(value.(OrganizerEvent)) {
			list = append(list, value.(OrganizerEvent))
		}
		return true
	})

	return list
}
