package event

import (
	"errors"
	"time"
)

// ErrNoEventFound is returned when a specified event cannot be found.
var ErrNoEventFound = errors.New("no event found")

// Store defines the interface to store organizer events.
type Store interface {
	// Add adds the given event to the store.
	Add(OrganizerEvent) error

	// Update updates the given event in the store. Returns an error if the event does not exist beforehand.
	Update(OrganizerEventUpdate) error

	// Get returns an event by its ID, or an empty event with the appropriate error if no event is found with the given ID.
	Get(id string) (OrganizerEvent, error)

	// List all the events in the store.
	List(filter func(OrganizerEvent) bool) []OrganizerEvent
}

// OrganizerEvent matches the structure of the event from an Organizer perspective.
type OrganizerEvent struct {
	// ID to reference this specific event.
	ID string

	// Title to display for the event. Might not be unique.
	Title string

	// StartDate specifies the date and time at which the event starts.
	StartDate time.Time

	// EndDate specifies the date and time at which the event ends.
	EndDate time.Time

	// Location of the specified event (optional).
	Location struct {
		// Name of the location.
		Name string
		// Address (postal) of the location.
		Address string
	}

	// TicketsCount matches the number of available tickets.
	TicketsCount uint

	// MaximumTicketPerUser defines the maximum amount of tickets a user can buy.
	MaximumTicketPerUser uint

	// SaleStartDate defines the date and time at which the sale should start.
	SaleStartDate time.Time

	// LineUp of the event.
	LineUp []string

	// MediaURL should contain an URL to an image or a video to attach to the event.
	MediaURL string
}

// OrganizerEventUpdate is used to update an event (with the optional fields being nullable).
type OrganizerEventUpdate struct {
	// ID to reference this specific event.
	ID string

	// Title to display for the event. Might not be unique.
	Title *string

	// StartDate specifies the date and time at which the event starts.
	StartDate *time.Time

	// EndDate specifies the date and time at which the event ends.
	EndDate *time.Time

	// Location of the specified event (optional).
	Location struct {
		// Name of the location.
		Name *string
		// Address (postal) of the location.
		Address *string
	}

	// TicketsCount matches the number of available tickets.
	TicketsCount *uint

	// MaximumTicketPerUser defines the maximum amount of tickets a user can buy.
	MaximumTicketPerUser *uint

	// SaleStartDate defines the date and time at which the sale should start.
	SaleStartDate *time.Time

	// LineUp of the event.
	LineUp *[]string

	// MediaURL should contain an URL to an image or a video to attach to the event.
	MediaURL *string
}
