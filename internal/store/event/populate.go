package event

import (
	"encoding/csv"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
	"time"
)

var timeFormats = []string{
	"02/01/2006 15:04",
	"02/01/2006",
}

func parseDateTime(value string) (time.Time, error) {
	for _, layout := range timeFormats {
		t, err := time.Parse(layout, value)
		if err == nil {
			return t, nil
		}
	}
	return time.Time{}, fmt.Errorf("unable to parse date: %s", value)
}

// Populate the given store from the provided CSV source file (such as `./organizers-data.csv`).
func Populate(s Store, src string) error {
	file, err := os.Open(src)
	if err != nil {
		return fmt.Errorf("unable to open source file: %w", err)
	}
	defer file.Close()

	reader := csv.NewReader(file)

	// Read the file content

	reader.Read() // fast forward first line
	for {
		record, err := reader.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			return fmt.Errorf("unable to parse CSV file: %w", err)
		}

		if len(record) != 11 {
			return fmt.Errorf("misformatted CSV: unexpected line length, got %v columns but 11 were expected", len(record))
		}

		startDate, err := parseDateTime(record[2])
		if record[2] != "" && err != nil {
			return fmt.Errorf("unable to parse event start date: %w", err)
		}
		endDate, err := parseDateTime(record[3])
		if record[3] != "" && err != nil {
			return fmt.Errorf("unable to parse event end date: %w", err)
		}
		saleStartDate, err := parseDateTime(record[8])
		if record[8] != "" && err != nil {
			return fmt.Errorf("unable to parse event sale start date: %w", err)
		}
		ticketsCount, err := strconv.ParseInt(record[6], 10, 64)
		if record[6] != "" && err != nil {
			return fmt.Errorf("unable to parse event ticket count: %w", err)
		}
		maximumTicketPerUser, err := strconv.ParseInt(record[7], 10, 64)
		if record[7] != "" && err != nil {
			return fmt.Errorf("unable to parse event maximum ticket per user count: %w", err)
		}

		e := OrganizerEvent{
			ID:                   record[0],
			Title:                record[1],
			StartDate:            startDate,
			EndDate:              endDate,
			TicketsCount:         uint(ticketsCount),
			MaximumTicketPerUser: uint(maximumTicketPerUser),
			SaleStartDate:        saleStartDate,
			LineUp:               strings.Split(record[9], "-"),
			MediaURL:             record[10],
		}

		s.Add(e)
	}

	return nil
}
