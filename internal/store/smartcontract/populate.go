package smartcontract

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"time"
)

var timeFormats = []string{
	"02/01/2006 15:04",
	"02/01/2006",
}

// Populate the given store from the provided JSON source file (such as `./smart-contracts-data.json`).
func Populate(s Store, src string) error {
	file, err := os.Open(src)
	if err != nil {
		return fmt.Errorf("unable to open source file: %w", err)
	}
	defer file.Close()

	byteValue, _ := ioutil.ReadAll(file)

	var contracts []jsonContract
	json.Unmarshal(byteValue, &contracts)

	for _, c := range contracts {
		contract := Contract{
			EventID:        fmt.Sprint(c.EventID),
			CollectionName: c.CollectionName,
			CrowdsaleAddr:  c.SmartContract.CrowdsaleAddr,
			CollectionAddr: c.SmartContract.CollectionAddr,
			MultisigAddr:   c.SmartContract.MultisigAddr,
		}
		contract.SaleParams.IsPresale = c.SmartContract.SaleParams.IsPresale
		contract.SaleParams.MetadataList = c.SmartContract.SaleParams.MetadataList
		contract.SaleParams.PricePerToken = c.SmartContract.SaleParams.PricePerToken
		contract.SaleParams.MaxMintPerUser = c.SmartContract.SaleParams.MaxMintPerUser
		contract.SaleParams.SaleSize = c.SmartContract.SaleParams.SaleSize
		contract.SaleParams.SaleCurrency = c.SmartContract.SaleParams.SaleCurrency
		contract.SaleParams.StartTime = time.Unix(c.SmartContract.SaleParams.StartTime, 0)
		contract.SaleParams.EndTime = time.Unix(c.SmartContract.SaleParams.EndTime, 0)

		s.Add(contract)
	}

	return nil
}

type jsonContract struct {
	EventID        uint   `json:"event_id"`
	CollectionName string `json:"collection_name"`
	SmartContract  struct {
		CrowdsaleAddr  string `json:"crowdsale"`
		CollectionAddr string `json:"collection"`
		MultisigAddr   string `json:"multisig"`
		SaleParams     struct {
			IsPresale      bool           `json:"is_presale"`
			MetadataList   []any          `json:"metadata_list"`
			PricePerToken  float32        `json:"price_per_token"`
			MaxMintPerUser float32        `json:"max_mint_per_user"`
			SaleSize       uint           `json:"sale_size"`
			SaleCurrency   map[string]any `json:"sale_currency"`
			StartTime      int64          `json:"start_time"`
			EndTime        int64          `json:"end_time"`
		} `json:"sale_params"`
	} `json:"smart_contract"`
}
