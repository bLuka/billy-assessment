package smartcontract

import "time"

// Store defines the interface to store smartcontracts information.
type Store interface {
	// Add the contract to the store.
	Add(Contract) error

	// Get returns an contract by its ID, or an empty contract with the appropriate error if no event is found with the given ID.
	Get(string) []Contract
}

type Contract struct {
	EventID        string
	CollectionName string
	CrowdsaleAddr  string
	CollectionAddr string
	MultisigAddr   string
	SaleParams     struct {
		IsPresale      bool
		MetadataList   []any
		PricePerToken  float32
		MaxMintPerUser float32
		SaleSize       uint
		SaleCurrency   map[string]any
		StartTime      time.Time
		EndTime        time.Time
	}
}
