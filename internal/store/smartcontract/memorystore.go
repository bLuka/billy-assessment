package smartcontract

import "sync"

type memoryStore struct {
	m *sync.Map
}

// NewMemoryStore instanciates a new in-memory store (no data will be persisted!)
func NewMemoryStore() Store {
	return &memoryStore{
		m: new(sync.Map),
	}
}

func (s *memoryStore) Add(c Contract) error {
	s.m.Store(c.EventID+c.CollectionAddr, c)
	return nil
}

func (s *memoryStore) Get(id string) []Contract {
	list := make([]Contract, 0)

	s.m.Range(func(_, value interface{}) bool {
		if value.(Contract).EventID == id {
			list = append(list, value.(Contract))
		}
		return true
	})

	return list
}
