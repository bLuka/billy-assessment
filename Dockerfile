FROM golang:1.19 AS build
WORKDIR /go/src
COPY . .

ENV CGO_ENABLED=0
RUN go mod download

RUN go build -a -installsuffix cgo ./cmd/...

FROM scratch AS runtime

COPY --from=build /go/src/webserver ./
COPY --from=build /go/src/organizers-data.csv ./
COPY --from=build /go/src/smart-contracts-data.json ./

EXPOSE 8080/tcp
ENTRYPOINT ["./webserver"]
