# Event

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EventId** | **string** |  | [optional] [default to null]
**Title** | **string** |  | [optional] [default to null]
**StartDatetime** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**EndDateTime** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**Address** | **string** |  | [optional] [default to null]
**LocationName** | **string** |  | [optional] [default to null]
**TotalTicketsCount** | **int64** |  | [optional] [default to null]
**AssetUrl** | **string** |  | [optional] [default to null]
**LineUp** | **[]string** |  | [optional] [default to null]
**TicketCollections** | [***[]TicketCollectionInner**](array.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

