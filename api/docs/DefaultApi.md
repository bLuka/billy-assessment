# client{{classname}}

All URIs are relative to *http://localhost:8080/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**EventEventIDGet**](DefaultApi.md#EventEventIDGet) | **Get** /event/{eventID} | 
[**EventEventIDPatch**](DefaultApi.md#EventEventIDPatch) | **Patch** /event/{eventID} | 
[**EventsGet**](DefaultApi.md#EventsGet) | **Get** /events | 

# **EventEventIDGet**
> Event EventEventIDGet(ctx, eventID)


Returns the details of the given event, by ID.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **eventID** | **int64**| Sale start time to filter events list with. The response will only contain events with a starting sale starting _after_ the given time (Epoch UNIX timestamp, in second).  | 

### Return type

[**Event**](Event.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EventEventIDPatch**
> Event EventEventIDPatch(ctx, body)


Update the details of the given event, by ID. Omitted fields will not be updated. Calling this route twice is idempotent. 

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **body** | [**PatchEventPayload**](PatchEventPayload.md)|  | 

### Return type

[**Event**](Event.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EventsGet**
> EventsGet(ctx, optional)


Returns a list of events

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
 **optional** | ***DefaultApiEventsGetOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a DefaultApiEventsGetOpts struct
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **saleStartTime** | **optional.Int64**| Sale start time to filter events list with. The response will only contain events with a starting sale starting _after_ the given time (Epoch UNIX timestamp, in second).  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

