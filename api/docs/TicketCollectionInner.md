# TicketCollectionInner

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**CollectionName** | **string** |  | [optional] [default to null]
**ScAddress** | **string** |  | [optional] [default to null]
**CollectionAddress** | **string** |  | [optional] [default to null]
**PricePerToken** | **float32** |  | [optional] [default to null]
**MaxMintPerUser** | **float32** |  | [optional] [default to null]
**SaleSize** | **int64** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

